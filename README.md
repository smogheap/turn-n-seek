turn & seek
===========

A mindless, infinite game of dial-turning.


How to play
-----------

- Peek around to see the goal pattern (in the center).
- Turn the dials to match.

http://turn-n-seek.penduin.net